const axios = require('axios');

exports.additionalInformation = async (roomID, userInfo) => {
    console.log('room id => ', roomID)
    console.log('userInfo => ', userInfo)
    const payload = {
      user_properties: userInfo,
    };
    console.log(`payload: ${payload}`);
  
    const url = `https://multichannel.qiscus.com/api/v1/qiscus/room/${roomID}/user_info`;
    //key akun staging lama
    const headers = {
      Authorization: 'gb6yxvVjWGEhXKsxNJuq',
    };
    const newHeaders = {
      Authorization: 'WZWhmJoNACpNypwPlfkL'
    }
    //sampai sini
    
    const newHeadersHalosisTrial = {
      Authorization: 'HwfdbM3dJKIrwZFV8r2i'
    }
  
    axios.post(url, payload, { headers: headers }).then((result) => {
      return console.log('axios run for old qiscus...', result.data);
    }
    );
  
    axios.post(url, payload, { headers: newHeaders }).then((result) => {
      return console.log('axios run for old qiscus...', result.data);
    }
    );
  
    axios.post(url, payload, { headers: newHeadersHalosisTrial }).then((result) => {
      return console.log('axios run for old qiscus...', result.data);
    }
    );
  };