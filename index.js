/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
const qiscusIinfo = require('./qiscusInformation');

const qiscus_caa = async function (req, res){
  try{
    console.log('start...');
    const qiscus_info = await qiscusIinfo.qiscusInformation(req, res)
    res.send(qiscus_info);
  }catch (err) {
    console.log('error log : ', err)
    return false;
  }
}
qiscus_caa();