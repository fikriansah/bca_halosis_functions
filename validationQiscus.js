const formatrupiah = require('./formatRupiah');

exports.validationQiscus = (result, result_get) => {
    if (
        result.data.payload.data === "data empty" ||
        result.data.payload.data.length === 0
      ) {
        return false
      } else if (result.data.payload.data.orders.length === 0) {
        return false
      } else {
        let payload = [
          {
            key: 'Order ID',
            value: result.data.payload.data.orders[0].id === null || result.data.payload.data.orders[0].id === "" || result.data.payload.data.orders[0].id === undefined ? "-" : result.data.payload.data.orders[0].id,
          },
          {
            key: 'Nama',
            value: name,
          },
          {
            key: 'Alamat',
            value: `${result.data.payload.data.customer_addresses[0].address_name}, ${result.data.payload.data.customer_addresses[0].address_detail} ${result.data.payload.data.customer_addresses[0].address}`,
          },
          {
            key: 'No Hp',
            value: email,
          },
          {
            key: 'order number',
            value: `${result.data.payload.data.orders[0].order_number}`,
          },
          {
            key: 'status order',
            value: `${result.data.payload.data.orders[0].status}`,
          },
          {
            key: 'jumlah produk',
            value: `${result.data.payload.data.orders[0].order_items.length}`,
          },
          {
            key: 'shipment',
            value: `${result.data.payload.data.orders[0].shipping_method}`,
          },
          {
            key: 'Total belanja',
            value: `Rp. ${formatRupiah(parseInt(result.data.payload.data.orders[0].total_price) + parseInt(result.data.payload.data.orders[0].delivery_price))}`,
          },
          {
            key: 'Link_Payment',
            value: `${PAYMENT_URL}${result.data.payload.data.orders[0].id}`,
          },
          {
            key: 'No. Resi',
            value: result.data.payload.data.orders[0].tracking_code === null ? "-" : result.data.payload.data.orders[0].tracking_code,
          },
          {
            key: 'Data Whatsapp',
            value: result_get.data.payload.data.length > 0 ? `https://stg.np.halosis.cloud/+${email}` : 'Tidak ada history chat'
          }
        ];
        return { result : payload }
      }
}