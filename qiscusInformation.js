const axios = require('axios');
const validation = require('./validation');
const validationQiscus = require('./validationQiscus');
const additionalInformation = require('./additionalInformation');


exports.qiscusInformation = (req, res) => {
    const body = req.body;
    const roomID = body.room_id;
    const email = body.email;
    const name = body.name;
    let data_key = key.findIndex(result => result.app_id == req.body.app_id)
    const url = `https://multichannel.qiscus.com/api/v1/admin/get_profile`;
  
    const BEARERTOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5FSkRSRUUxT1RRNFEwRXdRamc0UTBJM1JqUkVRamhCT1RGRVFUaEZRVEl6TnpCQlFURXhOZyJ9.eyJpc3MiOiJodHRwczovL2Rldi1zYWhhYmF0aGFsb3Npcy5hdXRoMC5jb20vIiwic3ViIjoiaEhIdDlNMXk1ZjQyd3I3SGVKVElSczRKbDVDSTVFckpAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vc2gtYXBpIiwiaWF0IjoxNTY4NTk1MDYyLCJleHAiOjE1NzExODcwNjIsImF6cCI6ImhISHQ5TTF5NWY0MndyN0hlSlRJUnM0Smw1Q0k1RXJKIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.uzw6-SSxfs4UmcSfKoXdBUtJWnOG_IW6Z7LbENC2NfmlsOp-0YeopRjcQSFBM3tEIRQlbTdfhIZSGznYODfzIdPMlSSl62YKNGky-W1yVrvA71PtTZKvZV1GwUy_5buTk0dUGPSYEfCJTVnaV_OsZwYKkFCEkw9v28MZBtlb7l5kdu210dDRRsi4P6mb4-vSF8loCOdQ8RSvbkEX-xrEvzsc6ep93tqymA2pDbPrj29kmyDh4Xi57f5LQRdbWqbTnnkhbHVf9xsu0O83pM04c_QJpXJkFeMwVCbNAXmYVBJjNU3BJqEGF-uowbsuXEdjIVj13sqneIsJyv0oj0AXKg==halosis"
    const PAYMENT_URL = "https://pay.halosis.co.id/"
    const url_additional = `https://multichannel.qiscus.com/api/v1/qiscus/room/${req.body.room_id}/user_info`
    const Headers = {
      Authorization: key[data_key].authorization,
      'Content-Type': 'application/json',
      'Qiscus-App-Id': key[data_key].app_id
    }
    axios.get(url_additional, { headers: Headers }).then(result => {
      if (result.data.data.extras.user_properties.length > 0) {
        const request = {
          additional: result.data.data.extras.user_properties,
          qapp_id: key[data_key].app_id,
          abc: req.body
        }
        axios.post(`https://api.v3.halosis.co.id/v3/qiscus/additional`, request, { headers: { Authorization: `Bearer ${BEARERTOKEN}` } })
          .then(result_save => {
            console.log("result save => ", result_save.data.payload.data)
          })
      }
      axios.get(`https://api.v3.halosis.co.id/orders/last/customer/${email}`, { headers: { Authorization: `Bearer ${BEARERTOKEN}` } })
        .then((result) => {
          axios.get(`https://stg.api.halosis.co.id/v3/customers/get/data_chat/${email}`, { headers: { Authorization: `Bearer ${BEARERTOKEN}` } })
            .then(result_get => {
              let result_validation = validation.validation(result, result_get)
              if(result_validation!=false){
                additionalInformation(roomID, result_validation)
              }
              res.send("sukses...")
            })
        })
    }).catch(err => {
      res.send(err)
      axios.get(`https://api.v3.halosis.co.id/orders/last/customer/${email}`, { headers: { Authorization: `Bearer ${BEARERTOKEN}` } })
        .then((result) => {
          axios.get(`https://stg.api.halosis.co.id/v3/customers/get/data_chat/${email}`, { headers: { Authorization: `Bearer ${BEARERTOKEN}` } })
            .then(result_get => {
              let result_validationQiscus = validationQiscus.validationQiscus(result, result_get)
              if(result_validationQiscus!=false){
                additionalInformation(roomID, result_validationQiscus)
              }
              res.send("sukses...")
            })
        })
    })
  }